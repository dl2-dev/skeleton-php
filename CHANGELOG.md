# Changelog

## [Unreleased]

Not yet released; provisionally 1.0.0 (may change).

### Added

### Changed

### Removed

[unreleased]: https://bitbucket.org/%ORGNAME%/%REPOSITORY%/branches/compare/HEAD..1.0.0
